package com.epam.edp.demo.conroller;

import com.epam.edp.demo.service.GreetingService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Pavlo_Yemelianov
 */
@RestController
public class HelloEdpController {

    GreetingService greetingService;

    @GetMapping(value = "/api/hello")
    public String hello() {
        return greetingService.getRandomGreeting();
    }

}
