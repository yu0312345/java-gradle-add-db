package com.epam.edp.demo.service;


public interface GreetingService {

    String getRandomGreeting();
}
